module github.com/mwitkow/grpc-proxy

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/stretchr/testify v1.3.0
	gitlab.com/pokstad1/grpc-proxy v0.0.0-20181017164139-0f1106ef9c76
	golang.org/x/net v0.0.0-20190424112056-4829fb13d2c6
	google.golang.org/grpc v1.20.1
)
